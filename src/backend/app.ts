import * as express from 'express';

import * as bodyParser from 'body-parser';
import {createServer} from 'http';
import {join} from 'path';
import * as socketIO from 'socket.io';

import * as connectLiveReload from 'connect-livereload';

import chat from './chat';

const PORT = process.env.PORT || 3000;
const app = express();
const server = createServer(app);
const io = socketIO(server);

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(connectLiveReload({
  port: 35729,
}));

app.use(express.static(join(__dirname, 'public')));

app.get('/', (req, res) => {
  res.sendFile(join(__dirname, 'public', 'index.html'));
});

chat(io);

server.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
