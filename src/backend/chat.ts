import {Observable, Subject} from '@reactivex/rxjs';
import {List, Map} from 'immutable';

import {
    IResult, ISockMessage, IState, IUserInfo, IUserMessage,
} from '../common/iuser';

import {
    handleChangeColor, handleDelete, handleLogin, handleLogout, handlePost,
    makeStreamFromConnection, parseChangeColorMessage, parseDeleteMessage,
    parseLoginMessage, parsePostMessage,
} from './util';

const chat = (io: SocketIO.Server) => {
    // refactor
    const [validLoginRequestStream, invalidLoginRequestStream] =
        makeStreamFromConnection(io, 'login', parseLoginMessage)
            .partition((v: IResult<ISockMessage>) => v.success);

    const [validPostMessageStream, invalidPostMessageStream] =
        makeStreamFromConnection(io, 'post', parsePostMessage)
            .partition((v: IResult<ISockMessage>) => v.success);

    const [validDeleteMessageStream, invalidDeleteMessageStream] =
        makeStreamFromConnection(io, 'deleteMessage', parseDeleteMessage)
            .partition((v: IResult<ISockMessage>) => v.success);

    const [validChangeColorStream, invalidChangeColorStream] =
        makeStreamFromConnection(io, 'changeColor', parseChangeColorMessage)
            .partition((v: IResult<ISockMessage>) => v.success);

    const validLogoutStream = makeStreamFromConnection(io, 'logout');

    const invalidRequestsStream = new Subject<ISockMessage>();

    Observable.merge(invalidPostMessageStream, invalidLoginRequestStream,
        invalidDeleteMessageStream, invalidChangeColorStream)
        .map((f: IResult<ISockMessage>) => f.value)
        .subscribe(invalidRequestsStream);

    const validRequestsStream = Observable.merge(validLoginRequestStream,
        validDeleteMessageStream, validPostMessageStream,
        validChangeColorStream, validLogoutStream)
        .map((s: IResult<ISockMessage>) => s.value);

    invalidRequestsStream.subscribe((s) => console.log('reject', s.message));

    validRequestsStream.map((sm: ISockMessage) => {
        return (state: IState): IState => {
            switch (sm.action) {
                case 'login':
                    return handleLogin(sm, state);
                case 'post':
                    return handlePost(sm, state);
                case 'deleteMessage':
                    return handleDelete(sm, state);
                case 'changeColor':
                    return handleChangeColor(sm, state);
                case 'logout':
                    return handleLogout(sm, state);
                case 'disconnect':
                    return handleLogout(sm, state);
                default:
                    return state;
            }
        };
    })
    .scan((state: IState,
           changeFunc: (state: IState) => IState) => changeFunc(state), {
            dest: null,
            users: Map<string, IUserInfo>(),
            messages: List<IUserMessage>(),
        } as IState)
        .pluck('dest')
        .subscribe((dest: ISockMessage) => {
            const action = dest.action;
            const message = dest.message;
            const socket = dest.socket;
            socket.emit(action, message);
        });
};

export default chat;
