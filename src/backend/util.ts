import { Observable, Subscriber } from '@reactivex/rxjs';
// import * as _ from 'lodash';

import {
    IResult, ISockMessage, IState, IUserInfo,
    IUserMessage,
} from '../common/iuser';

export const MESSAGE_TIMEOUT = 10 * 60 * 1000;
export const AUTOLOGOUT_TIMEOUT = 300000;

export const parseMessage = (sm: ISockMessage, checkFunction?: (value: any) => boolean): IResult<ISockMessage> => {
    const socket = sm.socket;
    const action = sm.action;

    try {
        const message = JSON.parse(sm.message);
        if (typeof checkFunction === 'function' && !checkFunction(message)) {
            return {
                success: false,
                value: {
                    socket,
                    action: 'reject',
                    message: JSON.stringify('Invalid message format'),
                } as ISockMessage,
            } as IResult<ISockMessage>;
        }

        return {
            success: true,
            value: { socket, action, message } as ISockMessage,
        } as IResult<ISockMessage>;

    } catch (e) {
        return {
            success: false,
            value: {
                socket,
                action: 'reject',
                message: JSON.stringify('Unknown message format'),
            } as ISockMessage,
        } as IResult<ISockMessage>;
    }
};

export const isLoginMessage = (value: any): boolean =>
    value && typeof value.username === 'string' && value.username !== '';

export const isPostMessage = (value: any): boolean =>
    value && typeof value === 'string' && value !== '';

export const isChangeColorMessage = (value: any): boolean =>
    value && typeof value === 'string' && value !== '';

export const isDeleteMessage = (value: any): boolean => {
    return value && typeof value === 'number' && value.toString() !== '';
};

export const parseLoginMessage = (sm: ISockMessage): IResult<ISockMessage> =>
    parseMessage(sm, isLoginMessage);

export const parsePostMessage = (sm: ISockMessage): IResult<ISockMessage> =>
    parseMessage(sm, isPostMessage);

export const parseChangeColorMessage = (sm: ISockMessage):
    IResult<ISockMessage> =>
    parseMessage(sm, isChangeColorMessage);

export const parseDeleteMessage = (sm: ISockMessage): IResult<ISockMessage> =>
    parseMessage(sm, isDeleteMessage);

export const makeStreamFromConnection = (io: SocketIO.Server, connType: string,
                                         parseFunction?: (sm: ISockMessage) => IResult<ISockMessage>):
    Observable<IResult<ISockMessage>> => {

    const connectionStream = Observable.create(
        (subscriber: Subscriber<SocketIO.Socket>) =>
            io.on('connection', (socket: SocketIO.Socket) =>
                subscriber.next(socket)));

    return connectionStream.mergeMap((socket: SocketIO.Socket) =>
        Observable.fromEvent(socket, connType)
            .map((message: string) =>
                typeof parseFunction === 'function' ?
                    parseFunction({
                        socket,
                        action: connType,
                        message,
                    } as ISockMessage)
                    : {
                        success: true,
                        value: {
                            socket,
                            action: connType,
                            message,
                        } as ISockMessage,
                    } as IResult<ISockMessage>,
            ));
};

export const isLoggedIn = (id: string, state: IState):
    (IUserInfo | undefined) =>
    state.users.get(id);

export const isUsernameTaken = (username: string, state: IState): boolean =>
    state.users.valueSeq()
        .findIndex((v: IUserInfo) => v.username === username) >= 0;

export const handleLogin = (sm: ISockMessage, state: IState): IState => {
    const id = sm.socket.id;
    const username = sm.message.username;
    const users = state.users;

    // refactor
    if (isLoggedIn(id, state)) {
        return Object.assign({}, state, {
            dest: {
                socket: sm.socket,
                action: 'reject',
                message: JSON.stringify('You\'re allready logged in'),
            },
        });
    }

    if (isUsernameTaken(username, state)) {
        return Object.assign({}, state, {
            dest: {
                socket: sm.socket,
                action: 'reject',
                message: JSON.stringify('Sorry, username taken'),
            },
        });
    } else {
        return Object.assign({}, state, {
            dest: {
                socket: sm.socket,
                action: 'login',
                message: JSON.stringify({
                    username,
                    allMessages: state.messages,
                }),
            },
            users: users.set(id, { username } as IUserInfo),
        });
    }
};

export const handlePost = (sm: ISockMessage, state: IState): IState => {
    const socket = sm.socket;
    const messages = state.messages;
    const currentUser = isLoggedIn(socket.id, state);

    if (currentUser) {
        const newMessage = {
            username: currentUser.username,
            timestamp: Date.now(),
            text: sm.message,
            messageColor: currentUser.messageColor || 'black',
        } as IUserMessage;
        return Object.assign({}, state, {
            dest: {
                socket: socket.server,
                action: 'post',
                message: JSON.stringify(newMessage),
            },
            messages: messages.push(newMessage),
        });
    } else {
        return Object.assign({}, state, {
            dest: {
                socket, action: 'reject',
                message: JSON.stringify('Sorry, you\'re not logged in'),
            },
        });
    }
};

export const handleDelete = (sm: ISockMessage, state: IState): IState => {
    const socket = sm.socket;
    const messages = state.messages;
    const currentUser = isLoggedIn(socket.id, state);
    const timestamp = parseInt(sm.message, 10);

    if (Date.now() - timestamp > MESSAGE_TIMEOUT) {
        return Object.assign({}, state, {
            dest: {
                socket, action: 'reject',
                message: JSON.stringify(`You cant delete messages that are
                                         more than 15 minutes old`),
            },
        });
    }

    if (currentUser) {
        const idx = messages.findIndex((message: IUserMessage) =>
            message.timestamp === timestamp);
        if (idx >= 0 && currentUser.username === messages.get(idx).username) {
            return Object.assign({}, state, {
                dest: {
                    socket: socket.server,
                    action: 'deleteMessage',
                    message: JSON.stringify(timestamp),
                },
                messages: messages.delete(idx),
            });
        } else {
            return Object.assign({}, state, {
                dest: {
                    socket,
                    action: 'reject',
                    message: JSON.stringify(`Message doesnt exists or
                                             it's too old`),
                },
            });
        }
    } else {
        return Object.assign({}, state, {
            dest: {
                socket,
                action: 'reject',
                message: JSON.stringify('You\'re not logged in'),
            },
        });
    }
};

export const handleLogout = (sm: ISockMessage, state: IState): IState => {
    const id = sm.socket.id;
    const users = state.users;
    const loggedIn = isLoggedIn(id, state);

    return Object.assign({}, state, {
        dest: {
            socket: sm.socket,
            action: loggedIn ? 'logout' : 'reject',
            message: loggedIn ?
                JSON.stringify('You\'re logged out successfully')
                : JSON.stringify('Sorry, you\'re not logged in'),
        },
        users: users.delete(id),
    });
};

export const handleChangeColor = (sm: ISockMessage, state: IState): IState => {
    const socket = sm.socket;
    const users = state.users;
    const currentUser = isLoggedIn(socket.id, state);
    const color = sm.message;

    if (currentUser) {

        return Object.assign({}, state, {
            dest: {
                socket, action: 'changeColor',
                message: JSON.stringify(`From now on, your message color will
                                         be ${color}`),
            },
            users: users.set(socket.id, {
                username: currentUser.username,
                messageColor: color,
            } as IUserInfo),
        });
    } else {
        return Object.assign({}, state, {
            dest: {
                socket,
                action: 'reject',
                message: JSON.stringify('Sorry, you\'re not logged in'),
            },
        });
    }
};
