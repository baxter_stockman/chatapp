declare function Velocity(element: HTMLElement,
                          type: string,
                          {duration : number}): Promise<HTMLElement>;


interface Properties {
  width?: number;
  height?: number;
  opacity?: number;
  top?: 'string',
  bottom?: 'string',
  left?: 'string',
  right?: 'string'
  padding?: 'string',
  margin?: 'string'
}

interface Options {
   /* Velocity's default options */
   container?: HTMLElement;
   duration?:number;
   delay?: number;
   easing?:string;
   queue?:string;
   begin?: (elements: HTMLElement[]) => void;
   progress?: (elements: HTMLElement[],
               complete: number,
               remaining: number,
               start: number,
               tweenValue: number) => void;
   complete?: (elements: HTMLElement[]) => void;
   display?: string,
   visibility?: string,
   loop?: boolean,
   mobileHA?: boolean
}


declare function Velocity(element: HTMLElement, properties:Properties,
  options?: Options): Promise<HTMLElement>;

declare function Velocity(element: HTMLElement, action: string, options?: Options)
  : Promise<HTMLElement>;
