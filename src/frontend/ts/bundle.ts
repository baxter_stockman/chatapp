import { Observable } from '@reactivex/rxjs';
import './velocity.ts';


import { IUserMessage } from '../../common/iuser';

import { MESSAGE_TIMEOUT } from '../../backend/util';

const disableInputs = (...elements: HTMLElement[]) => {
    elements.forEach((e) => {
        e.setAttribute('disabled', 'true');
    });
};

const enableInputs = (...elements: HTMLElement[]) => {
    elements.forEach((e) => {
        e.removeAttribute('disabled');
    });
};

const checkInput = (input: HTMLInputElement, button: HTMLElement) => {
    (input.value.trim() !== '') ?
        enableInputs(button)
        : disableInputs(button);
};

const addMessage = (parent: HTMLElement, postMessage: IUserMessage,
                    username: string, socket: SocketIOClient.Socket):
    { timestamp: number, entry: HTMLElement } => {

    const timestamp = postMessage.timestamp;

    const li = document.createElement('li');
    const div = document.createElement('div');
    div.classList.add('chatMessage');
    const img = document.createElement('img');
    img.setAttribute('src', '/img/avatar.png');
    const messageInfo = document.createElement('span');
    messageInfo.style.fontStyle = 'italic';
    messageInfo.style.backgroundColor = '#EFEFEF';
    const time = (new Date(timestamp)).toString().split(' ').slice(0, -2)
        .join(' ');
    messageInfo.textContent = `${postMessage.username} on ${time} wrote:`;
    const messageContent = document.createElement('span');
    messageContent.style.color = postMessage.messageColor || 'black';
    messageContent.style.display = 'block';
    messageContent.textContent = `${postMessage.text}`;
    div.appendChild(img);
    div.appendChild(messageInfo);
    div.appendChild(messageContent);

    if (Date.now() - timestamp < MESSAGE_TIMEOUT &&
        username === postMessage.username) {
        const deleteButton = document.createElement('button');
        deleteButton.textContent = 'X';
        deleteButton.classList.add('deleteButton');
        messageContent.appendChild(deleteButton);
        Observable.fromEvent(deleteButton, 'click')
            .subscribe((evt: Event) => {
                socket.emit('deleteMessage', JSON.stringify(timestamp));
            });
        setTimeout(() => messageContent.removeChild(deleteButton),
            MESSAGE_TIMEOUT);
    }

    li.appendChild(div);
    parent.appendChild(li);
    Velocity(li, 'scroll', {
        container: parent,
        duration: 900,
    });
    return { timestamp, entry: li };
};

Observable.fromEvent(document, 'DOMContentLoaded')
    .subscribe(() => {
        const socket = io();
        const loginCard = document.querySelector('#loginCard') as HTMLElement;
        const loginField =
            document.querySelector('#loginField') as HTMLInputElement;
        loginField.focus();
        const loginButton =
            document.querySelector('#loginButton') as HTMLElement;

        checkInput(loginField, loginButton);

        const postCard = document.querySelector('#chatCard') as HTMLElement;
        const postField =
            document.querySelector('#postField') as HTMLInputElement;
        const postButton = document.querySelector('#postButton') as HTMLElement;
        checkInput(postField, postButton);

        const colorButton =
            document.querySelector('#colorButton') as HTMLElement;
        const colorOptions =
            document.querySelector('.colorOptions') as HTMLElement;

        const logoutButton =
            document.querySelector('#logoutButton') as HTMLElement;
        const logoutConfirm =
            document.querySelector('.logoutConfirm') as HTMLElement;
        const logoutYes = document.querySelector('#logoutYes') as HTMLElement;
        const logoutNo = document.querySelector('#logoutNo') as HTMLElement;

        const messageBox = document.querySelector('.messageBox') as HTMLElement;
        const messageList =
            document.querySelector('.messageList') as HTMLElement;

        const redColorSelect = document.querySelector('#red') as HTMLElement;
        const greenColorSelect = document.querySelector('#green') as HTMLElement;
        const blueColorSelect = document.querySelector('#blue') as HTMLElement;

        const notifyBox = document.querySelector('.notifyBox') as HTMLElement;
        const notifyText =
            document.querySelector('.notifyBox span') as HTMLElement;

        let myUsername = '';
        const postedMessages: Array<{ timestamp: number, entry: HTMLElement }> =
            [];

        Observable.fromEvent(loginField, 'keyup')
            .debounceTime(200)
            .subscribe((evt: Event) =>
                checkInput(evt.target as HTMLInputElement, loginButton));

        Observable.fromEvent(loginButton, 'click')
            .subscribe((evt: Event) => {
                evt.preventDefault();
                socket.emit('login', JSON.stringify({
                    username: loginField.value,
                }));
            });

        Observable.fromEvent(socket, 'login')
            .subscribe((messages: string) => {
                colorOptions.style.display = 'none';
                logoutConfirm.style.display = 'none';
                disableInputs(logoutButton, colorButton, postButton, postField);
                Velocity(loginCard, 'fadeOut', { duration: 300 })
                    .then(() => {
                        Velocity(postCard, 'fadeIn', { duration: 300 });
                        let allMessages: IUserMessage[] = [];
                        const response = JSON.parse(messages) as {
                            username: string,
                            allMessages: IUserMessage[],
                        };
                        allMessages = response.allMessages;

                        allMessages.forEach((message: IUserMessage) =>
                            postedMessages.push(addMessage(messageList,
                                message, myUsername, socket)));

                        myUsername = response.username;
                        setTimeout(() => {
                            const lastMessage = document
                                .querySelector('.messageList li:last-child') as HTMLElement;
                            Velocity(lastMessage, 'scroll', {
                                container: messageList,
                                duration: 900,
                            });
                            enableInputs(logoutButton, colorButton, postField);
                            postField.focus();
                        }, 300);
                    });
            });

        Observable.fromEvent(colorButton, 'click')
            .subscribe((evt: Event) => {
                evt.preventDefault();

                if (colorOptions.style.display === 'none') {
                    Velocity(colorOptions, 'slideDown', { duration: 300 });
                    disableInputs(logoutButton, postField, postButton);
                    messageBox.classList.add('greyText');
                } else {
                    Velocity(colorOptions, 'slideUp', { duration: 300 })
                        .then(() => {
                            enableInputs(logoutButton, colorButton, postField);
                            if (postField.value) {
                                enableInputs(postButton);
                            }
                            postField.focus();
                            messageBox.classList.remove('greyText');
                        });
                }
            });

        Observable.fromEvent(logoutButton, 'click')
            .subscribe((evt: Event) => {
                evt.preventDefault();
                if (logoutConfirm.style.display === 'none') {
                    Velocity(logoutConfirm, 'slideDown', { duration: 300 });
                    disableInputs(logoutButton, colorButton, postButton,
                        postField);
                    messageBox.classList.add('greyText');
                }
            });

        Observable.fromEvent(logoutYes, 'click')
            .subscribe((evt: Event) => {
                evt.preventDefault();
                socket.emit('logout');
            });

        Observable.merge(
            Observable.fromEvent(socket, 'logout'),
            Observable.fromEvent(socket, 'disconnect'),
        ).subscribe((message: string) => {
            logoutButton.removeAttribute('disabled');
            postField.value = '';
            myUsername = '';
            Velocity(postCard, 'fadeOut', { duration: 300 })
                .then(() => {
                    while (messageList.firstChild) {
                        messageList.removeChild(messageList.firstChild);
                    }
                    Velocity(loginCard, 'fadeIn', { duration: 300 });
                    colorOptions.style.display = 'none';
                    logoutConfirm.style.display = 'none';
                    enableInputs(logoutButton, colorButton, postButton,
                        postField);
                    messageBox.classList.remove('greyText');
                    notifyText.textContent = JSON.parse(message) as string;
                    Velocity(notifyBox, 'fadeIn', { delay: 500, duration: 300 })
                    .then(() => {
                        Velocity(notifyBox, 'fadeOut', {
                            delay: 1500,
                            duration: 300,
                        });
                    });
                });
        });


        Observable.fromEvent(logoutNo, 'click')
            .subscribe((evt: Event) => {
                Velocity(logoutConfirm, 'slideUp', { duration: 300 })
                    .then(() => {
                        enableInputs(logoutButton, colorButton, postField);
                        if (postField.value) {
                            enableInputs(postButton);
                        }
                        postField.focus();
                        messageBox.classList.remove('greyText');
                    });
            });

        Observable.fromEvent(postField, 'keyup')
            .debounceTime(200)
            .subscribe((evt: Event) =>
                checkInput(evt.target as HTMLInputElement, postButton));

        Observable.fromEvent(postButton, 'click')
            .subscribe((evt: Event) => {
                evt.preventDefault();
                socket.emit('post', JSON.stringify(postField.value));
            });

        Observable.fromEvent(socket, 'post')
            .subscribe((message: string) => {
                const newMessage = JSON.parse(message) as IUserMessage;
                postedMessages.push(addMessage(messageList, newMessage,
                    myUsername, socket));
                setTimeout(() => postField.focus(), 900);
            });


        Observable.fromEvent(socket, 'reject')
            .subscribe((message: string) => {
                notifyText.textContent = JSON.parse(message);
                Velocity(notifyBox, 'fadeIn', { duration: 300 })
                    .then(() => {
                        Velocity(notifyBox, 'fadeOut', {
                            delay: 1500,
                            duration: 300,
                        });
                    });
            });

        Observable.fromEvent(redColorSelect, 'click')
            .subscribe((evt: Event) => {
                evt.preventDefault();
                socket.emit('changeColor', JSON.stringify('red'));
            });

        Observable.fromEvent(greenColorSelect, 'click')
            .subscribe((evt: Event) => {
                evt.preventDefault();
                socket.emit('changeColor', JSON.stringify('green'));
            });

        Observable.fromEvent(blueColorSelect, 'click')
            .subscribe((evt: Event) => {
                evt.preventDefault();
                socket.emit('changeColor', JSON.stringify('blue'));
            });

        Observable.fromEvent(socket, 'changeColor')
            .subscribe((message: string) => {
                notifyText.textContent = JSON.parse(message);
                Velocity(notifyBox, 'fadeIn', { duration: 300 })
                    .then(() => {
                        Velocity(notifyBox, 'fadeOut', {
                            delay: 1500,
                            duration: 300,
                        });
                    });
            });

        Observable.fromEvent(socket, 'deleteMessage')
            .subscribe((message: string) => {
                const timestamp = JSON.parse(message) as number;
                const id = postedMessages.find((pm) =>
                    pm.timestamp === timestamp);

                if (id) {
                    messageList.removeChild(id.entry);
                }
            });

    });
