import { List, Map } from 'immutable';

export interface ISockMessage {
    socket: SocketIO.Socket;
    action: string;
    message: any;
}

export interface IParsedObject {
    [property: string]: string;
}

export interface IUserInfo {
    username: string;
    messageColor?: string;
}

export interface IUserMessage {
    username: string;
    timestamp: number;
    text: string;
    messageColor?: string;
}

export interface IState {
    dest: ISockMessage | null;
    users: Map<string, IUserInfo>;
    messages: List<IUserMessage>;
}

export interface IResult<T> {
    success: boolean;
    value: T;
}
