const gulp = require('gulp');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const browserify = require("browserify");
const watchify = require("watchify");
const source = require('vinyl-source-stream');
const tsify = require("tsify");
const gutil = require("gulp-util");
const sass = require('gulp-sass');
const gls = require('gulp-live-server');
const path = require('path');

const BACKEND_SRC = path.join(__dirname, 'src', 'backend', '**', '*.ts');
const BACKEND_DEST = path.join(__dirname, 'dist');

const FRONTEND_SRC = path.join(__dirname, 'src', 'frontend', 'ts', 'bundle.ts');
const FRONTEND_DEST = path.join(__dirname, 'dist', 'public', 'js');

const ASSETS_SRC = path.join(__dirname, 'src', 'assets', '**', '*');
const ASSETS_DEST = path.join(__dirname, 'dist', 'public');

const TYPINGS_SRC = path.join(__dirname, 'typings', '*.d.ts');
const SCSS_SRC = path.join(__dirname, 'src', 'frontend',
  'scss', '**', '*.scss');

const SCSS_DEST = path.join(__dirname, 'dist', 'public', 'css');

const watchedBrowserify = browserify({
  basedir: __dirname,
  debug: true,
  entries: FRONTEND_SRC,
  cache: {},
  packageCache: {},
  plugin: [tsify]
}).plugin(watchify, {
  ignoreWatch: [path.join(__dirname, 'node_modules', '**'),
  path.join(__dirname, 'dest', '**'),
  path.join(__dirname, 'src', 'backend', '**')]
});

const bundle = () =>
  watchedBrowserify
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest(FRONTEND_DEST));

gulp.task('backend', () =>
  gulp.src([BACKEND_SRC, TYPINGS_SRC])
    .pipe(sourcemaps.init())
    .pipe(ts({
      module: 'commonjs',
      sourceMap: true,
      noImplicitAny: true,
      target: 'es6'
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(BACKEND_DEST)));

gulp.task('frontend', () => bundle());

gulp.task('sass', () =>
  gulp.src(SCSS_SRC)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(SCSS_DEST)));

gulp.task('watch:src', () => {
  gulp.watch(SCSS_SRC, ['sass']);
  gulp.watch(BACKEND_SRC, ['backend']);
  // gulp.watch(FRONTEND_SRC, ['frontend']);
});

gulp.task('copy:assets', () => {
  gulp.src(ASSETS_SRC).pipe(gulp.dest(ASSETS_DEST));
});

gulp.task('serve', ['copy:assets', 'backend', 'frontend', 'sass'], () => {
  watchedBrowserify.on("update", bundle);
  watchedBrowserify.on("log", gutil.log);
  gulp.watch(BACKEND_SRC, ['backend']);
  gulp.watch(SCSS_SRC, ['sass']);
  const server = gls.new(path.join(__dirname, 'dist', 'app.js'),
    {env: {NODE_ENV: 'development'}});
  server.start();
  gulp.watch(path.join(__dirname, 'dist', 'public', '*.html'), file => {
    server.notify.apply(server, [file]);
  });

  gulp.watch(path.join(SCSS_DEST, '*.css'), file =>
    server.notify.apply(server, [file]));

  gulp.watch(path.join(FRONTEND_DEST, '*.js'), file =>
    server.notify.apply(server, [file]));

  gulp.watch(path.join(__dirname, 'dist', '*.js'), () => {
    server.start.bind(server)();
  });
});
